const sql = require('mssql');
const {sqlConfig} = require('../util/database')

exports.loadFormData = async (req, res) => {
    try{
        let reqData = req.body;
        console.log(req.body);
        let query = `select * from CodeGen 
					inner join ProjectLead on CodeGen.ProjectLeadID = ProjectLead.ProjectLeadID
					inner join Frontend on CodeGen.FrontendID = Frontend.FrontendID
					inner join ServiceApi on CodeGen.ServiceID = ServiceApi.ServiceID
					inner join DatabaseApi on CodeGen.DatabaseID = DatabaseApi.DatabaseID where ${reqData.ColumnName}=${reqData.MetaData}

 `
        let db= await sql.connect(sqlConfig);
        let result = await db.request()
                             .query(query);
        sql.close();
        console.log(result.recordsets);
        let responseData = result.recordsets;
        res.json({
            Success:true,
            ResponseData : responseData,
            ErrorMessage : null
        })
    }
    catch(err){
        console.log(err);
        res.json({
            Success:false,
            ResponseData : null,
            ErrorMessage : err.message
        })
    }
}


exports.outputFileUpdate = async(req, res) => {
        try{
            let db= await sql.connect(sqlConfig);
        let result;
        let reqData = req.body;
        let CodeGenID = req.body.RequiredID;
        console.log(reqData);
        
             result = await db.query(` update  CodeGen set CodeStatus=${reqData.CodeStatus},ModifiedAt = getDate(), GeneratedCodePath= '${reqData.OutputFilename}' where CodeGenID = ${reqData.RequiredID}
             `)
        
        
        
        
        sql.close();
        console.log(result); 
        let responseData = result.rowsAffected;
        //console.log(responseData);
        res.json({
            Success:true,
            ResponseData : responseData,
            ErrorMessage : null
        })
        }
        catch(err){
            console.log(err);
            res.json({
                Success:false,
                ResponseData : null,
                ErrorMessage : err.message
            })
        }
}

exports.fetchFormData = async (req, res) => {
    try{
        //console.log('KJDHVLISDGIO');
        
        let db= await sql.connect(sqlConfig);
        let result = await db.request()
                             .query(`
									select * from ProjectLead
									select * from Frontend
									select * from ServiceApi
									select * from DatabaseApi`);
        sql.close();
        //console.log(result); 
        let responseData = result.recordsets;
        //console.log(responseData);
        res.json({
            Success:true,
            ResponseData : responseData,
            ErrorMessage : null
        })
    }
    catch(err){
    console.log(err);
        res.json({
            Success:false,
            ResponseData : null,
            ErrorMessage : err.message
        })
    }
}

exports.SubmitInfo = async (req, res) => {
    try{
        //console.log('KJDHVLISDGIO');
        let db= await sql.connect(sqlConfig);
        let result;
        let reqData = req.body;
        let CodeGenID = req.body.RequiredID;
        // console.log(`Insert into CodeGen(Project,Module,ComponentName,ComponentType,FrontendID, ServiceID, DatabaseID, ProjectLeadID, CodeStatus,InputExcelPath )  
        // values ('${reqData.ProjectName}','${reqData.ModuleName}','${reqData.ComponentName}','${reqData.ComponentType}',${reqData.Frontend},${reqData.Service},${reqData.Database},${reqData.TeamLead},${reqData.CodeStatus} ,'${reqData.Filename}'
        //          )`);
        if (CodeGenID=="0"){
             result = await db.request()
                             .query(` Insert into CodeGen(Project,Module,ComponentName,ComponentType,FrontendID, ServiceID, DatabaseID, ProjectLeadID, CodeStatus,InputExcelPath )  
                             values ('${reqData.ProjectName}','${reqData.ModuleName}','${reqData.ComponentName}','${reqData.ComponentType}',${reqData.Frontend},${reqData.Service},${reqData.Database},${reqData.TeamLead},0,'${reqData.Filename}'
                                      )
                             select Top 1 CodeGenID from CodeGen order by CodeGenID Desc `);
        }
        if (CodeGenID!="0"){
             result = await db.request().query(` update  CodeGen set Project='${reqData.ProjectName}',Module='${reqData.ModuleName}',ComponentName='${reqData.ComponentName}',ComponentType='${reqData.ComponentType}',FrontendID=${reqData.Frontend}, ServiceID=${reqData.Service}, DatabaseID=${reqData.Database}, ProjectLeadID=${reqData.TeamLead}, CodeStatus=0, InputExcelPath = '${reqData.Filename}' , ModifiedAt = getDate()  where CodeGenID = ${reqData.RequiredID}
            select  CodeGenID from CodeGen where CodeGenID = ${reqData.RequiredID} `)
        }
        
        
        
        sql.close();
        console.log(result); 
        let responseData = result.recordsets;
        //console.log(responseData);
        res.json({
            Success:true,
            ResponseData : responseData,
            ErrorMessage : null
        })
    }
    catch(err){
    console.log(err);
        res.json({
            Success:false,
            ResponseData : null,
            ErrorMessage : err.message
        })
    }
}