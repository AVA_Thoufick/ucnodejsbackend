const sql = require('mssql');
exports.loadFormData = async (context, req) => {
    try {
        let reqData = req.body;
        console.log(req.body);
        let query = `select * from CodeGen 
					inner join Frontend on CodeGen.FrontendID = Frontend.FrontendID
					inner join ServiceApi on CodeGen.ServiceID = ServiceApi.ServiceID
					inner join DatabaseApi on CodeGen.DatabaseID = DatabaseApi.DatabaseID where ${reqData.ColumnName}=${reqData.MetaData}

 `
        let db = await sql.connect({

            user: 'cdgennpadmin',
            password: "qJ]%='L.<6t6;YY2",
            database: 'ava-eus-cdgen-np-sqldb',
            server: 'ava-eus-cdgen-np-sqlsrv.database.windows.net',
            // or
            // server: 'SEZLP366\\SQLEXPRESS',
            options: {
                encrypt: true, // for azure
                trustServerCertificate: true // change to true for local dev / self-signed certs
            }
        });
        let result = await db.request()
            .query(query);
        sql.close();
        console.log(result.recordsets);
        let responseData = result.recordsets;
        context.res = {
            body: {
                Success: true,
                ResponseData: responseData,
                ErrorMessage: null
            },
            headers: {
                'Access-Control-Allow-Credentials': 'true',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'GET',
                'Content-Type': 'application/json',
                'Access-Control-Request-Headers': '*'

            }
        }
    }
    catch (err) {
        console.log(err);
        context.res = {
            body: {
                Success: false,
                ResponseData: err.message,
                ErrorMessage: null
            },
            headers: {
                'Access-Control-Allow-Credentials': 'true',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'GET',
                'Content-Type': 'application/json',
                'Access-Control-Request-Headers': '*'

            }
        }
    }

}
